#!/bin/bash

git clone https://github.com/francielizanon/agios.git
cd agios
mkdir build
cd build
cmake ..
make
make install
ldconfig
